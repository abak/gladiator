# Gladiator

A developer tool to generate UI definition files from a running GTK application.

## Known issues

* Some values cannot be retrieved:
   * `GtkInfoBar` response ids
   * `GtkLevelBar` offset names
   * `GtkTreeViewColumn` attributes
   * `PangoAttribute` values
* Property bindings
* Signal handlers and closures
