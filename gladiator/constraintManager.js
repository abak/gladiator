/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { GLib, GObject, Gtk } = imports.gi;
const System = imports.system;
import { FunctionMenuButton } from './menuButton.js';
import { getEnumClasses } from './objects/classes.js';
import { getExplicitBuildableId } from './objects/id.js';

function enumToString(type, value) {
    let Enum = getEnumClasses().find(Klass => Klass.$gtype == type);
    let key = Object.keys(Enum).find(key => Enum[key] == value);

    return key?.split('_').map(w => w[0] + w.slice(1).toLowerCase()).join(' ') ?? String(value);
}

function gObjectToString(gObject) {
    let name = gObject.name || getExplicitBuildableId(gObject);
    return `${gObject.constructor.$gtype.name}${name ? ' - ' + name : ''}`;
}

const AddressFactory = GObject.registerClass({
}, class extends Gtk.SignalListItemFactory {
    on_setup(listItem) {
        listItem.child = new Gtk.Label({ xalign: 0 });
    }

    on_bind(listItem) {
        listItem.child.label = System.addressOfGObject(listItem.item);
    }
});

const PSpecFactory = GObject.registerClass({
    Properties: {
        'pspec': GObject.ParamSpec.param(
            'pspec', "ParamSpec", "The param spec to display the value for",
            GObject.ParamFlags.READWRITE, GObject.TYPE_PARAM
        ),
    },
}, class extends Gtk.SignalListItemFactory {
    on_setup(listItem) {
        listItem.child = new Gtk.Label({ xalign: 0 });
    }

    on_bind(listItem) {
        if (this.pspec.name == 'strength') {
            listItem.child.label = enumToString(Gtk.ConstraintStrength.$gtype, listItem.item[this.pspec.name]);
        } else if (GObject.type_is_a(this.pspec.value_type, GObject.TYPE_ENUM)) {
            listItem.child.label = enumToString(this.pspec.value_type, listItem.item[this.pspec.name]);
        } else if (GObject.type_is_a(this.pspec.value_type, GObject.TYPE_OBJECT)) {
            if (listItem.item[this.pspec.name]) {
                listItem.child.label = System.addressOfGObject(listItem.item[this.pspec.name]);
                listItem.child.tooltipText = gObjectToString(listItem.item[this.pspec.name]);
            } else {
                listItem.child.label = "None";
            }
        } else {
            listItem.child.label = String(listItem.item[this.pspec.name]);
        }
    }
});

export default GObject.registerClass({
    Properties: {
        'inspected-object': GObject.ParamSpec.object(
            'inspected-object', "Inspected object", "The inspected layout whose constraints this displays",
            GObject.ParamFlags.READWRITE, GObject.TYPE_OBJECT
        ),
    },
}, class extends Gtk.Box {
    _init(params) {
        super._init(Object.assign({
            orientation: Gtk.Orientation.VERTICAL,
            spacing: 10,
        }, params));

        this._guideView = new Gtk.ColumnView({
            model: new Gtk.NoSelection(),
        });

        this._guideView.append_column(new Gtk.ColumnViewColumn({
            title: "Guide",
            factory: new AddressFactory(),
        }));

        GObject.Object.list_properties.call(Gtk.ConstraintGuide).forEach(pspec => {
            this._guideView.append_column(new Gtk.ColumnViewColumn({
                title: pspec.name,
                factory: new PSpecFactory({ pspec }),
            }));
        });

        this.append(new Gtk.ScrolledWindow({
            propagateNaturalHeight: true,
            child: this._guideView,
        }));

        this._removeGuideButton = new FunctionMenuButton({
            iconName: 'list-remove-symbolic',
            tooltipText: "Remove guide",
        });
        this._removeGuideButton.popover.position = Gtk.PositionType.RIGHT;
        this._removeGuideButton.set_create_popup_func(menuButton => {
            menuButton.setParams([
                { name: "guide", instances: this.inspectedObject.observe_guides() },
            ]);
        });
        this._removeGuideButton.connect('apply', (b_, f_, [guide]) => {
            this.inspectedObject.remove_guide(guide);
            this._update();
        });

        this._addGuideButton = new FunctionMenuButton({
            iconName: 'list-add-symbolic',
            tooltipText: "Add guide",
        });
        this._addGuideButton.popover.position = Gtk.PositionType.RIGHT;
        this._addGuideButton.set_create_popup_func(menuButton => {
            menuButton.setParams([
                { name: "min_width", "numeric": true, lower: 0, upper: GLib.MAXINT32, value: 0 },
                { name: "min_height", "numeric": true, lower: 0, upper: GLib.MAXINT32, value: 0 },
                { name: "nat_width", "numeric": true, lower: 0, upper: GLib.MAXINT32, value: 0 },
                { name: "nat_height", "numeric": true, lower: 0, upper: GLib.MAXINT32, value: 0 },
                { name: "max_width", "numeric": true, lower: 0, upper: GLib.MAXINT32, value: GLib.MAXINT32 },
                { name: "max_height", "numeric": true, lower: 0, upper: GLib.MAXINT32, value: GLib.MAXINT32 },
                { name: "strength", enum: 'GtkConstraintStrength', value: 'medium' },
                { name: "name", text: true },
            ]);
        });
        this._addGuideButton.connect('apply', (b_, f_, args) => {
            let [minWidth, minHeight, natWidth, natHeight, maxWidth, maxHeight, strength, name] = args;
            this.inspectedObject.add_guide(new Gtk.ConstraintGuide({
                minWidth, minHeight, natWidth, natHeight, maxWidth, maxHeight, strength, name: name || null,
            }));
            this._update();
        });

        let guideButtonBox = new Gtk.Box({ spacing: 6 });
        guideButtonBox.append(this._removeGuideButton);
        guideButtonBox.append(this._addGuideButton);
        this.append(guideButtonBox);

        this._constraintView = new Gtk.ColumnView({
            model: new Gtk.NoSelection(),
        });

        this._constraintView.append_column(new Gtk.ColumnViewColumn({
            title: "Constraint",
            factory: new AddressFactory(),
        }));

        GObject.Object.list_properties.call(Gtk.Constraint).forEach(pspec => {
            this._constraintView.append_column(new Gtk.ColumnViewColumn({
                title: pspec.name,
                factory: new PSpecFactory({ pspec }),
            }));
        });

        this.append(new Gtk.ScrolledWindow({
            propagateNaturalHeight: true,
            child: this._constraintView,
        }));

        this._removeConstraintButton = new FunctionMenuButton({
            iconName: 'list-remove-symbolic',
            tooltipText: "Remove constraint",
        });
        this._removeConstraintButton.popover.position = Gtk.PositionType.RIGHT;
        this._removeConstraintButton.set_create_popup_func(menuButton => {
            menuButton.setParams([
                { name: "constraint", instances: this.inspectedObject.observe_constraints() },
            ]);
        });
        this._removeConstraintButton.connect('apply', (b_, f_, [constraint]) => {
            this.inspectedObject.remove_constraint(constraint);
            this._update();
        });

        this._addConstraintButton = new FunctionMenuButton({
            iconName: 'list-add-symbolic',
            tooltipText: "Add constraint",
        });
        this._addConstraintButton.popover.position = Gtk.PositionType.RIGHT;
        this._addConstraintButton.set_create_popup_func(menuButton => {
            menuButton.setParams([
                { name: "target", instances: this._getTargets(), nullable: true },
                { name: "target_attribute", enum: 'GtkConstraintAttribute', value: 'none' },
                { name: "relation", enum: 'GtkConstraintRelation', value: 'eq' },
                { name: "source", instances: this._getTargets(), nullable: true },
                { name: "source_attribute", enum: 'GtkConstraintAttribute', value: 'none' },
                { name: "multiplier", "numeric": true, digits: 3, lower: Number.MIN_SAFE_INTEGER, upper: Number.MAX_SAFE_INTEGER, value: 1 },
                { name: "constant", "numeric": true, digits: 3, lower: Number.MIN_SAFE_INTEGER, upper: Number.MAX_SAFE_INTEGER, value: 0 },
                { name: "strength", enum: 'GtkConstraintStrength', value: 'required' },
            ]);
        });
        this._addConstraintButton.connect('apply', (b_, f_, args) => {
            // args: [target, targetAttribute, relation, source, sourceAttribute, multiplier, constant, strength]
            this.inspectedObject.add_constraint(Gtk.Constraint.new(...args));
            this._update();
        });

        let constraintButtonBox = new Gtk.Box({ spacing: 6 });
        constraintButtonBox.append(this._removeConstraintButton);
        constraintButtonBox.append(this._addConstraintButton);
        this.append(constraintButtonBox);
    }

    _getTargets() {
        let guides = [], guideListModel = this.inspectedObject.observe_guides();
        for (let i = 0; i < guideListModel.get_n_items(); i++) {
            let guide = guideListModel.get_item(i);
            guides.push(guide);
        }
        return guides.concat([...this.inspectedObject.get_widget()]);
    }

    _update() {
        this._guideView.model.model = this.inspectedObject?.observe_guides() ?? null;
        this._removeGuideButton.sensitive = !!this._guideView.model.model?.get_n_items();
        this._addGuideButton.sensitive = !!this.inspectedObject;

        this._constraintView.model.model = this.inspectedObject?.observe_constraints() ?? null;
        this._removeConstraintButton.sensitive = !!this._constraintView.model.model?.get_n_items();
        this._addConstraintButton.sensitive = !!this.inspectedObject;
    }

    get inspectedObject() {
        return this._inspectedObject ?? null;
    }

    set inspectedObject(inspectedObject) {
        if (!(inspectedObject instanceof Gtk.ConstraintLayout))
            inspectedObject = null;

        if (this.inspectedObject == inspectedObject)
            return;

        this._inspectedObject = inspectedObject;
        this.notify('inspected-object');

        this._update();
    }
});
