/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gio, GObject, Gtk } = imports.gi;
const System = imports.system;
const pgettext = imports.gettext.domain('gtk40').pgettext;
import { GridPopover } from './menuButton.js';
import { getExplicitBuildableId } from './objects/id.js';

const MODES = ["None", "Horizontal", "Vertical", "Both"];

// Maintain a list of size groups since _gtk_widget_get_sizegroups is private.
Gtk.SizeGroup.sizeGroups = new Set();
Gtk.SizeGroup.prototype.addWidgetOld = Gtk.SizeGroup.prototype.add_widget;
Gtk.SizeGroup.prototype.add_widget = function(widget) {
    this.addWidgetOld(widget);
    Gtk.SizeGroup.sizeGroups.add(this);
};
Gtk.Widget.prototype.getSizeGroups = function() {
    return [...Gtk.SizeGroup.sizeGroups]
        .filter(sizeGroup => sizeGroup.get_widgets().includes(this));
};

export default GObject.registerClass({
    Properties: {
        'inspected-object': GObject.ParamSpec.object(
            'inspected-object', "Inspected object", "The inspected widget to manage the size groups for",
            GObject.ParamFlags.READWRITE, GObject.TYPE_OBJECT
        ),
    },
    Signals: {
        'inspected-object-grouped': {},
    }
}, class extends Gtk.Box {
    _init(params) {
        super._init(Object.assign({
            spacing: 6,
        }, params));

        this._newSizeGroup = new Gtk.SizeGroup();

        this._removeButton = new Gtk.MenuButton({
            iconName: 'list-remove-symbolic',
            tooltipText: "Remove from a size group",
        });
        this._removeButton.set_create_popup_func(this._setRemovePopover.bind(this));
        this.append(this._removeButton);

        this._addButton = new Gtk.MenuButton({
            iconName: 'list-add-symbolic',
            tooltipText: "Add to a size group",
        });
        this._addButton.set_create_popup_func(this._setAddPopover.bind(this));
        this.append(this._addButton);
    }

    _setRemovePopover(menuButton) {
        let popover = new GridPopover();

        let store = new Gio.ListStore({ itemType: Gtk.SizeGroup.$gtype });
        let sizeGroups = this.inspectedObject.getSizeGroups();
        store.splice(0, 0, sizeGroups);

        let sizeGroupDropDown = new Gtk.DropDown({
            model: store,
            expression: new Gtk.ClosureExpression(GObject.TYPE_STRING, sizeGroup => {
                let string = System.addressOfGObject(sizeGroup);
                if (getExplicitBuildableId(sizeGroup))
                    string += ` (${getExplicitBuildableId(sizeGroup)})`;

                return string;
            }, null),
        });

        popover.addRow("Size Group", sizeGroupDropDown);
        popover.connect('apply', () => {
            let sizeGroup = sizeGroupDropDown.selectedItem;

            popover.popdown();
            sizeGroup.remove_widget(this.inspectedObject);
            this.emit('inspected-object-grouped');
        });

        menuButton.set_popover(popover);
    }

    _setAddPopover(menuButton) {
        let popover = new GridPopover();

        let store = new Gio.ListStore({ itemType: Gtk.SizeGroup.$gtype });
        let sizeGroups = [...Gtk.SizeGroup.sizeGroups]
            .filter(sizeGroup => !sizeGroup.get_widgets().includes(this.inspectedObject));
        store.splice(0, 0, sizeGroups);
        store.append(this._newSizeGroup);

        let sizeGroupDropDown = new Gtk.DropDown({
            model: store,
            expression: new Gtk.ClosureExpression(GObject.TYPE_STRING, sizeGroup => {
                if (sizeGroup == this._newSizeGroup)
                    return "New";

                let string = System.addressOfGObject(sizeGroup);
                if (getExplicitBuildableId(sizeGroup))
                    string += ` (${getExplicitBuildableId(sizeGroup)})`;

                return string;
            }, null),
        });
        popover.addRow("Size Group", sizeGroupDropDown);

        let ModeDropDown = new Gtk.DropDown({
            model: Gtk.StringList.new(MODES),
            expression: new Gtk.ClosureExpression(GObject.TYPE_STRING, stringObject => {
                return pgettext('sizegroup mode', stringObject.string);
            }, null),
            selected: 1,
        });

        sizeGroupDropDown.connect('notify::selected-item', () => {
            if (sizeGroupDropDown.selectedItem == this._newSizeGroup)
                popover.addRow("Mode", ModeDropDown);
            else
                popover.removeRow(1);
        });
        sizeGroupDropDown.notify('selected-item');

        popover.connect('apply', () => {
            let sizeGroup = sizeGroupDropDown.selectedItem;
            if (sizeGroup == this._newSizeGroup) {
                this._newSizeGroup.set_mode(ModeDropDown.selected);
                this._newSizeGroup = new Gtk.SizeGroup();
            }

            popover.popdown();
            sizeGroup.add_widget(this.inspectedObject);
            this.emit('inspected-object-grouped');
        });

        menuButton.set_popover(popover);
    }

    _update() {
        this._removeButton.sensitive = (
            this.inspectedObject  &&
            ([...Gtk.SizeGroup.sizeGroups]?.some(sizeGroup => sizeGroup.get_widgets().includes(this.inspectedObject)) ?? false)
        );

        this._addButton.sensitive = !!this.inspectedObject;
    }

    get inspectedObject() {
        return this._inspectedObject ?? null;
    }

    set inspectedObject(inspectedObject) {
        if (!(inspectedObject instanceof Gtk.Widget) || (this.inspectedObject instanceof Gtk.Window))
            inspectedObject = null;

        if (this.inspectedObject == inspectedObject)
            return;

        this._inspectedObject = inspectedObject;
        this.notify('inspected-object');

        this._update();
    }
});
