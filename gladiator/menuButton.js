/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gio, GLib, GObject, Gtk } = imports.gi;
const System = imports.system;
import { getEnumClasses, getObjectClasses } from './objects/classes.js';
import { getExplicitBuildableId } from './objects/id.js';

Gtk.test_register_all_types();

const EnumMemberObject = GObject.registerClass({
    Properties: {
        'key': GObject.ParamSpec.string(
            'key', "Key", "The key of the enumeration member",
            GObject.ParamFlags.READWRITE, null
        ),
        'value': GObject.ParamSpec.int(
            'value', "Value", "The value of the enumeration member",
            GObject.ParamFlags.READWRITE, -GLib.MAXINT32, GLib.MAXINT32, 0
        ),
    },
}, class extends GObject.Object {});

const enumToStore = function(Enum) {
    let store = new Gio.ListStore({ itemType: EnumMemberObject.$gtype });
    let enumMemberObjects = Object.entries(Enum).sort(([, a], [, b]) => a - b).map(([key, value]) => {
        return new EnumMemberObject({ key: key.toLowerCase().replace(/_/g, '-'), value });
    });
    store.splice(0, 0, enumMemberObjects);
    store.getPositionForKey = key => enumMemberObjects.findIndex(item => item.key == key);
    return store;
};

const ConstructorObject = GObject.registerClass({
    Properties: {
        'js-constructor': GObject.ParamSpec.jsobject(
            'js-constructor', "JS Constructor", "The constructor",
            GObject.ParamFlags.READWRITE
        ),
    },
}, class extends GObject.Object {});

const ABSTRACTS = [Gtk.ListItemFactory];

const typesToStore = function(typeNames = [], excludedNames = [], activatable = false) {
    // First call getObjectClasses() to be sure all (e.g. Adw) types are registered
    // before calling GObject.type_from_name().
    let constructors = getObjectClasses();

    let types = typeNames.map(name => GObject.type_from_name(name));
    let excluded = excludedNames.map(name => GObject.type_from_name(name));
    let items = constructors.filter(constructor => (
        !ABSTRACTS.includes(constructor) &&
        types.some(type => type && GObject.type_is_a(constructor.$gtype, type)) &&
        excluded.every(type => !type || !GObject.type_is_a(constructor.$gtype, type)) &&
        (!activatable || Gtk.Widget.get_activate_signal.call(constructor))
    )).map(constructor => new ConstructorObject({ jsConstructor: constructor }));

    let store = new Gio.ListStore({ itemType: ConstructorObject.$gtype });
    store.splice(0, 0, items);
    store.findPositionForTypeName = function(typeName) {
        let item = items.find(item => item.jsConstructor?.$gtype.name == typeName);
        if (item) {
            let [success, position] = store.find(item);
            if (success)
                return position;
        }

        return null;
    };

    return store;
};

export const GridPopover = GObject.registerClass({
    Signals: {
        'apply': {},
    }
}, class extends Gtk.Popover {
    _init(params) {
        let vbox = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            marginTop: 5, marginBottom: 5,
            marginStart: 5, marginEnd: 5,
            spacing: 10,
        });

        this._grid = new Gtk.Grid({
            rowSpacing: 5, columnSpacing: 5,
        });
        vbox.append(this._grid);

        let cancelButton = new Gtk.Button({
            halign: Gtk.Align.START,
            label: "Cancel",
        });
        cancelButton.connect('clicked', () => this.popdown());

        this._applyButton = new Gtk.Button({
            halign: Gtk.Align.END,
            hexpand: true,
            label: "Apply",
        });
        this._applyButton.connect('clicked', () => this.emit('apply'));

        let hbox = new Gtk.Box({
            spacing: 5,
        });
        hbox.append(cancelButton);
        hbox.append(this._applyButton);
        vbox.append(hbox);

        super._init(Object.assign({
            child: vbox,
        }, params));
    }

    addRow(label, widget) {
        let labelWidget = new Gtk.Label({ label, xalign: 1 });
        this._grid.attach_next_to(labelWidget, null, Gtk.PositionType.BOTTOM, 1, 1);
        this._grid.attach_next_to(widget, labelWidget, Gtk.PositionType.RIGHT, 1, 1);
    }

    getRowWidget(position) {
        return this._grid.get_child_at(1, position);
    }

    removeRow(position) {
        this._grid.remove_row(position);
    }
});

export const FunctionMenuButton = GObject.registerClass({
    Signals: {
        'apply': {
            param_types: [GObject.TYPE_JSOBJECT, GObject.TYPE_JSOBJECT],
        },
    },
}, class extends Gtk.MenuButton {
    _init(params) {
        super._init(params);

        this.popover = new GridPopover();

        this.set_create_popup_func(() => {
            this._functionDropDown?.set_model(Gtk.StringList.new(this._functions.map(func => func.name.replace(/_/g, ' '))));
        });

        this.popover.connect('apply', () => {
            this.popdown();
            this.emit('apply', this._selectedFunction ?? null, this._getArgs());
        });
    }

    _getArgs() {
        let firstRow = this._functionDropDown ? 1 : 0;

        return this._params.map((param, index) => {
            let widget = this.popover.getRowWidget(firstRow + index);

            if (param.numeric) {
                return widget.value;
            } else if (param.text) {
                return widget.text;
            } else if (param.boolean) {
                return widget.active;
            } else if (param.enum) {
                return widget.selectedItem.value;
            } else if (param.instances) {
                if (param.nullable && (widget.selected == 0))
                    return null;

                return widget.selectedItem;
            } else {
                if (param.nullable && (widget.selected == 0))
                    return null;

                let constructor = widget.selectedItem.jsConstructor;
                return new constructor();
            }
        });
    }

    setFunctions(functions) {
        if (!this._functionDropDown) {
            this._functionDropDown = new Gtk.DropDown();
            this.popover.addRow("Function", this._functionDropDown);
            this._functionDropDown.connect('notify::selected-item', dropDown => {
                this._selectedFunction = this._functions.find(func => func.name.replace(/_/g, ' ') == dropDown.selectedItem?.string);
                this.setParams(this._selectedFunction?.params ?? []);
            });
        }

        this._functions = functions;
    }

    setParams(params) {
        let firstRow = this._functionDropDown ? 1 : 0;
        while (this.popover.getRowWidget(firstRow))
            this.popover.removeRow(firstRow);

        params.forEach((param, index) => {
            let widget;

            if (param.numeric) {
                widget = new Gtk.SpinButton({
                    widthChars: param.widthChars ?? 2,
                    numeric: true, digits: param.digits ?? 0,
                    adjustment: Gtk.Adjustment.new(param.value ?? 0, param.lower ?? 0, param.upper ?? GLib.MAXUINT32, 1, 10, 0)
                });
            } else if (param.text) {
                widget = new Gtk.Entry({
                    widthChars: param.widthChars ?? 7,
                });
            } else if (param.boolean) {
                widget = new Gtk.Switch({
                    halign: Gtk.Align.END,
                });
            } else if (param.enum) {
                let Enum = getEnumClasses().find(Klass => Klass.$gtype.name == param.enum);
                let store = enumToStore(Enum);

                widget = new Gtk.DropDown({
                    model: store,
                    expression: Gtk.PropertyExpression.new(store.get_item_type(), null, 'key'),
                    selected: param.value ? store.getPositionForKey(param.value) : 0,
                });
            } else if (param.instances) {
                let objects = param.instances;
                // objects is either an array or a list model.
                if (objects && objects instanceof Gio.ListModel) {
                    let model = objects;
                    objects = [];
                    for (let i = 0; i < model.get_n_items(); i++)
                        objects.push(model.get_item(i));
                }

                let store = new Gio.ListStore({ itemType: GObject.Object });
                store.splice(0, 0, objects);

                let nullGObject;
                if (param.nullable)
                    store.insert(0, nullGObject = new GObject.Object());

                widget = new Gtk.DropDown({
                    model: store,
                    expression: new Gtk.ClosureExpression(GObject.TYPE_STRING, gObject => {
                        if (gObject == nullGObject)
                            return "none";

                        let name = gObject.name || getExplicitBuildableId(gObject);
                        return `${System.addressOfGObject(gObject)} (${gObject.constructor.$gtype.name}${name ? ' - ' + name : ''})`;
                    }, null),
                });
            } else {
                let store = typesToStore(
                    param.types ?? ['GtkWidget'],
                    param.excluded ?? (param.types ? [] : ['GtkNative']),
                    param.activatableTypes
                );
                if (param.nullable)
                    store.insert(0, new ConstructorObject());

                widget = new Gtk.DropDown({
                    model: store,
                    expression: new Gtk.ClosureExpression(GObject.TYPE_STRING, constructorObject => {
                        return constructorObject.jsConstructor?.$gtype.name ?? "None";
                    }, null),
                    selected: (
                        param.default ? (store.findPositionForTypeName(param.default) ?? 0) :
                        !param.types && !param.nullable ? (store.findPositionForTypeName('GtkBox') ?? 0) :
                        0
                    ),
                });
            }

            let label = param.name.split('_').map(w => w[0].toUpperCase() + w.slice(1)).join(' ');
            this.popover.addRow(label, widget);
        });

        this._params = params;
    }
});
