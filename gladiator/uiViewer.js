/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gio, GLib, GObject, Gtk } = imports.gi;
const System = imports.system;
import { getExplicitBuildableId, getIdBase } from './objects/id.js';
import { uify } from './objects/ui.js';

const BASE_INDENTATION = 2;

const getAddress = function(object) {
    return System[object instanceof GObject.Object ? 'addressOfGObject' : 'addressOf'](object);
};

const getAttributes = function(attributeMap) {
    return Object.entries(attributeMap)
        .map(([attributeMap, value]) => ` ${attributeMap}="${value}"`)
        .join("");
};

// Buildable ids:
// Since the list of objects that need an id is unknown until all nodes are built,
// object addresses are used during the build and ultimately replaced by proper ids.
// The ids of an eventual initial builder file are preserved.
const UIContext = class {
    constructor(translationDomain) {
        this._idMap = new Map();
        this._idObjects = new Set();
        this._rootObjects = new Set();

        this._stack = [{ node: `<?xml version="1.0" encoding="UTF-8"?>` }];
        this.push("interface", { "domain": translationDomain ?? "gtk40" });
    }

    appendObject(object) {
        // Register the id if it is an explicit buildable id (it comes from a builder file).
        if (getExplicitBuildableId(object))
            this.registerId(object);

        let attributeMap = { "class": object.constructor.$gtype.name, "id": `@${getAddress(object)}@` };
        this.push("object", attributeMap);
        uify(object, this);
        this.pop();
    }

    appendMenu(object) {
        if (getExplicitBuildableId(object))
            this.registerId(object);

        let attributeMap = { "id": `@${getAddress(object)}@` };
        this.push("menu", attributeMap);
        uify(object, this);
        this.pop();
    }

    appendRoot(object) {
        if (this._rootObjects.has(object))
            return;

        this._rootObjects.add(object);

        let mainStack = this._stack;
        this._stack = [{}, { node: '' }];

        this.registerId(object);
        if (object instanceof Gio.MenuModel)
            this.appendMenu(object);
        else
            this.appendObject(object);

        mainStack[1].node += this._stack[1].node;
        this._stack = mainStack;
    }

    appendValue(element, attributeMap = {}, value = null, comment = false) {
        let indentation = " ".repeat((this._stack.length - 1) * BASE_INDENTATION);
        let attributes = getAttributes(attributeMap);
        let startComment = comment ? "<!--" : "";
        let endComment = comment ? "-->" : "";

        if (value instanceof Array && value.length > 1)
            value = `${value.join("\n")}\n${indentation}`;

        if (typeof value == 'string' && !(value.startsWith("<![CDATA")))
            value = GLib.markup_escape_text(value, -1);

        this._stack[this._stack.length - 1].node += '\n';
        this._stack[this._stack.length - 1].node += value === null ?
            `${indentation}${startComment}<${element}${attributes}/>${endComment}` :
            `${indentation}${startComment}<${element}${attributes}>${value}</${element}>${endComment}`;
    }

    push(element, attributeMap = {}, comment = false) {
        this._stack.push({ element, attributeMap, comment, node: "" });
    }

    pop() {
        let { element, attributeMap, comment, node } = this._stack.pop();
        let indentation = " ".repeat((this._stack.length - 1) * BASE_INDENTATION);
        let attributes = getAttributes(attributeMap);
        let startComment = comment ? "<!--" : "";
        let endComment = comment ? "-->" : "";

        this._stack[this._stack.length - 1].node += '\n';

        if (!node) {
            this._stack[this._stack.length - 1].node += `${indentation}${startComment}<${element}${attributes}/>${endComment}`;
            return;
        }

        this._stack[this._stack.length - 1].node += `${indentation}${startComment}<${element}${attributes}>`;
        this._stack[this._stack.length - 1].node += node;
        this._stack[this._stack.length - 1].node += '\n';
        this._stack[this._stack.length - 1].node += `${indentation}</${element}>${endComment}`;
    }

    // Signify that the object must have an id because an other object depends on it.
    registerId(object, id = getExplicitBuildableId(object)) {
        if (id)
            this._idMap.set(getAddress(object), id);
        else
            this._idObjects.add(object);
        return `@${getAddress(object)}@`;
    }

    getUI() {
        this.pop();
        let contents = this._stack[0].node;

        // Generate an id for the objects that don't have one.
        this._idObjects.forEach(object => {
            if (!this._idMap.has(getAddress(object))) {
                let id, i = 0, base = getIdBase(object);
                do {
                    id = `${base}${++i}`;
                } while([...this._idMap.values()].includes(id));

                this._idMap.set(getAddress(object), id);
            }
        });

        // Replace all the addresses ("@address@") by their corresponding id, if registered.
        return contents
            .replace(/@(\w*)@/g, (match_, address) => this._idMap.get(address) ?? "")
            .replace(/ id=""/g, "");
    }
};

export default GObject.registerClass({
    Properties: {
        'inspected-object': GObject.ParamSpec.object(
            'inspected-object', "Inspected object", "The inspected object whose UI this displays",
            GObject.ParamFlags.READWRITE, GObject.TYPE_OBJECT
        ),
        'translation-domain': GObject.ParamSpec.string(
            'translation-domain', "Translation domain", "The tranlation domain",
            GObject.ParamFlags.READWRITE, null
        ),
    },
}, class extends Gtk.Box {
    _init(params) {
        super._init(Object.assign({
            orientation: Gtk.Orientation.VERTICAL,
        }, params));

        let toolbar = new Gtk.CenterBox();
        let startBox = new Gtk.Box({
            marginStart: 6, marginEnd: 6,
            marginTop: 6, marginBottom: 6,
            spacing: 6,
        });
        let refreshButton = new Gtk.Button({
            hasFrame: false,
            iconName: 'view-refresh-symbolic',
            tooltipText: "Refresh UI",
        });
        refreshButton.connect('clicked', () => this.update());
        startBox.append(refreshButton);
        this._saveButton = new Gtk.Button({
            hasFrame: false,
            sensitive: false,
            iconName: 'document-save-symbolic',
            tooltipText: "Save the current UI",
        });
        this._saveButton.connect('clicked', () => this.save());
        startBox.append(this._saveButton);
        toolbar.set_start_widget(startBox);
        this.append(toolbar);

        this._textView = new Gtk.TextView({
            leftMargin: 6, rightMargin: 6,
            monospace: true,
        });

        this.append(new Gtk.ScrolledWindow({
            vexpand: true,
            child: this._textView,
        }));
    }

    get inspectedObject() {
        return this._inspectedObject ?? null;
    }

    set inspectedObject(inspectedObject) {
        if (this.inspectedObject == inspectedObject)
            return;

        this._inspectedObject = inspectedObject;
        this.notify('inspected-object');

        this.update();
    }

    save() {
        let contents = this._textView.buffer.text;
        if (!contents)
            return;

        let dialog = new Gtk.FileChooserNative({
            transientFor: this.get_root(), modal: true,
            action: Gtk.FileChooserAction.SAVE,
        });

        dialog.connect('response', (dialog_, response) => {
            let file;
            if (response == Gtk.ResponseType.ACCEPT)
                file = dialog.get_file();

            // FIXME: Offending 'items-changed' signal (probably on Gtk.Window.get_toplevels()).
            dialog.unref();
            dialog.destroy();

            if (file) {
                try {
                    file.replace_contents(contents, null, false, Gio.FileCreateFlags.NONE, null);
                } catch(e) {
                    log(e.message);
                }
            }
        });

        dialog.show();
        dialog.ref();
    }

    update() {
        if (this.inspectedObject) {
            let context = new UIContext(this.translationDomain);

            // Defined in sizeGroupButtons.js.
            [...Gtk.SizeGroup.sizeGroups]
                .filter(sizeGroup => !!sizeGroup.get_widgets().length)
                .forEach(sizeGroup => context.appendRoot(sizeGroup));

            context.registerId(this.inspectedObject);
            context.appendRoot(this.inspectedObject);

            this._textView.buffer.text = context.getUI();
        } else {
            this._textView.buffer.text = "";
        }

        this._saveButton.sensitive = !!this._textView.buffer.text;
    }
});
