/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gdk, Gio, GLib, GObject, Gtk, Pango } = imports.gi;
const ByteArray = imports.byteArray;
import { getChildren, isActivatable, searchWidgetForBuildableId } from './children.js';
import { getEnumClasses, getFlagsClasses } from './classes.js';
import { getExplicitBuildableId } from './id.js';
import { getCssClasses, getDefaultValue, isIdentified, isSkipped } from './properties.js';

let Adw;
try {
    Adw = imports.gi.Adw;
} catch(e) {}

const ROOT_CLASSES = [Gio.ListStore, Gio.Menu, Gtk.Adjustment, Gtk.EntryBuffer, Gtk.ListStore, Gtk.StringList, Gtk.TextBuffer, Gtk.TreeStore];

const $toValue = Symbol('toValue');
const $uifyProps = Symbol('uifyProps');
const $uifyCustoms = Symbol('uifyCustoms');
const $uifyChildren = Symbol('uifyChildren');

// Convert enum values to human-readable strings.
function enumToString(enumType, value) {
    let Enum = getEnumClasses().find(Klass => Klass.$gtype == enumType);
    if (!Enum)
        return null;

    let key = Object.entries(Enum).find(([k, v]) => v === value)?.[0];
    if (!key)
        return null;

    return key.toLowerCase().replace(/_/g, '-');
}

// Convert flags to human-readable strings.
function flagsToString(flagsType, value) {
    let Flags = getFlagsClasses().find(Klass => Klass.$gtype == flagsType);
    if (!Flags)
        return null;

    let keys = Object.entries(Flags).filter(([k, v]) => (typeof v) == 'number' && (value & v)).map(([k, v]) => k);
    if (!keys.length)
        return null;

    return keys.map(key => key.toLowerCase().replace(/_/g, '-')).join('|');
}

// Replace the 'instanceof' operator for optional library's types.
function instanceIsA(instance, ...typeNames) {
    return typeNames.some(typeName => (
        GObject.type_from_name(typeName) &&
        GObject.type_is_a(instance.constructor.$gtype, GObject.type_from_name(typeName))
    ));
}

Pango.Attribute.prototype.getTypeName = function() {
    let attrList = Pango.AttrList.new();
    attrList.insert(this.copy());
    let iterator = attrList.get_iterator();

    if (!iterator.get_attrs()[0])
        iterator.next();

    for (let key in Pango.AttrType) {
        let type = Pango.AttrType[key];
        if (typeof type != 'number')
            continue;

        if (iterator.get(type))
            return key.toLowerCase().replace(/_/g, '-');
    }

    return null;
};

// TODO: GdkPixbuf and GVariant (see GtkBuilder documentation).
Gdk.RGBA.prototype[$toValue] = function() {
    return this.to_string();
};

// Example: GtkBuilderListItemFactory:bytes.
GLib.Bytes.prototype[$toValue] = function() {
    let data = ByteArray.toString(this.get_data());
    if (!data)
        return null;

    return `<![CDATA[${data}]]>`;
};

GObject.Object.prototype[$uifyProps] = function(context) {
    let pspecs = GObject.Object.list_properties.call(this.constructor);
    pspecs.forEach(pspec => {
        if (!(pspec.flags & GObject.ParamFlags.READABLE) || !(pspec.flags & GObject.ParamFlags.WRITABLE))
            return;

        if (isSkipped(this, pspec.name))
            return;

        let value;
        try {
            value = this[pspec.name];
            let defaultValue = getDefaultValue(this, pspec);

            if (value == defaultValue || (!value && !defaultValue) || (value === null))
                return;

            if (Array.isArray(defaultValue) && (defaultValue.join() == value.join()))
                return;
        } catch(e) {
            // XXX: 'item-type' property of GListStore (see gtk-demo/listview_filebrowser.ui for example)
            if (pspec.value_type == GObject.TYPE_GTYPE && this instanceof Gio.ListModel) {
                value = this.get_item_type().name;
            } else {
                logError(e);
                return;
            }
        }

        if (GObject.type_is_a(pspec.value_type, GObject.TYPE_ENUM))
            value = enumToString(pspec.value_type, value) ?? value;
        else if (GObject.type_is_a(pspec.value_type, GObject.TYPE_FLAGS))
            value = flagsToString(pspec.value_type, value) ?? value;

        if (isIdentified(this, pspec.name))
            value = context.registerId(value);

        if (typeof value == 'object' && value[$toValue])
            value = value[$toValue]();

        if (ROOT_CLASSES.some(Klass => value instanceof Klass)) {
            context.appendRoot(value, true);
            value = context.registerId(value);
        }

        if (value instanceof GObject.Object) {
            let comment = (
                (this instanceof Gtk.DropDown || instanceIsA(this, 'AdwComboRow')) &&
                this.expression &&
                value instanceof Gtk.SignalListItemFactory
            );

            context.push("property", { "name": pspec.name }, comment);
            context.appendObject(value);
            context.pop();
        } else if (value instanceof Gtk.Expression) {
            context.push("property", { "name": pspec.name });
            uify(value, context);
            context.pop();
        } else if (typeof value != 'object' || value instanceof Array) {
            context.appendValue("property", { "name": pspec.name }, value);
        }
    });
};

// XXX: Can link iterator advance more than one time?
// XXX: How to retrieve menu ids?
Gio.Menu.prototype[$uifyCustoms] = function(context) {
    for (let i = 0; i < this.get_n_items(); i++) {
        let attributeIter = this.iterate_item_attributes(i);
        let linkIter = this.iterate_item_links(i);

        if (linkIter.next()) {
            context.push(linkIter.get_name());
            while (attributeIter.next()) {
                context.appendValue("attribute", { "name": attributeIter.get_name() }, attributeIter.get_value().unpack());
            }
            linkIter.get_value()[$uifyCustoms](context);
            context.pop();

            continue;
        }

        if (attributeIter.next()) {
            context.push("item");
            do {
                context.appendValue("attribute", { "name": attributeIter.get_name() }, attributeIter.get_value().unpack());
            } while (attributeIter.next());
            context.pop();
        }
    }
};

Gtk.Expression.prototype[$uifyCustoms] = function(context) {
};

Gtk.ClosureExpression.prototype[$uifyCustoms] = function(context) {
    context.push("closure", { "type": this.get_value_type().name, "function": "UNKNOWN" });
    context.appendValue("UNKNOWN", {}, null, true);
    context.pop();
};

Gtk.ConstantExpression.prototype[$uifyCustoms] = function(context) {
    let value = this.get_value();
    if (value instanceof GObject.Object)
        value = context.registerId(value);

    context.appendValue("constant", { "type": this.get_value_type().name }, value);
};

Gtk.PropertyExpression.prototype[$uifyCustoms] = function(context) {
    let pspec = this.get_pspec();
    let attributeMap = { "name": pspec.name, "type": pspec.owner_type.name };

    let expression = this.get_expression();
    if (expression && expression instanceof Gtk.ObjectExpression) {
        let object = expression.get_object();
        if (object)
            context.appendValue("lookup", attributeMap, context.registerId(object));
    } else if (expression) {
        context.push("lookup", attributeMap);
        uify(expression, context);
        context.pop();
    } else {
        context.appendValue("lookup", attributeMap);
    }
};

Gtk.Widget.prototype[$uifyCustoms] = function(context) {
    // Layout nodes.
    let layoutManager = this.get_parent()?.get_layout_manager();
    if (layoutManager instanceof Gtk.FixedLayout) {
        let layoutChild = layoutManager.get_layout_child(this);
        if (layoutChild.get_transform()) {
            context.push("layout");
            context.appendValue("property", { "name": "transform" }, layoutChild.get_transform().to_string());
            context.pop();
        }
    } else if (layoutManager instanceof Gtk.GridLayout) {
        let layoutChild = layoutManager.get_layout_child(this);
        let pspecs = GObject.Object.list_properties.call(Gtk.GridLayoutChild);
        context.push("layout");
        context.appendValue("property", { "name": "column" }, layoutChild.column);
        context.appendValue("property", { "name": "row" }, layoutChild.row);
        ['column-span', 'row-span']
            .filter(name => pspecs.find(pspec => pspec.name == name).default_value != layoutChild[name])
            .forEach(name => context.appendValue("property", { name }, layoutChild[name]));
        context.pop();
    } else if (layoutManager instanceof Gtk.OverlayLayout) {
        let layoutChild = layoutManager.get_layout_child(this);
        let pspecs = GObject.Object.list_properties.call(Gtk.OverlayLayoutChild);
        let propNames = ['clip-overlay', 'measure']
            .filter(name => pspecs.find(pspec => pspec.name == name).default_value != layoutChild[name]);
        if (propNames.length) {
            context.push("layout");
            propNames.forEach(name => context.appendValue("property", { name }, layoutChild[name]));
            context.pop();
        }
    }

    // Style nodes.
    let cssClasses = getCssClasses(this);
    if (cssClasses.length) {
        context.push("style");
        cssClasses.forEach(cssClasse => context.appendValue("class", { "name": cssClasse }));
        context.pop();
    }
};

Gtk.ConstraintLayout.prototype[$uifyCustoms] = function(context) {
    context.push("constraints");

    let guideList = this.observe_guides();
    let guides = Array.from({ length: guideList.get_n_items() }, (e_, i) => guideList.get_item(i));

    guides.sort((a, b) => {
        return (a.name ?? 'z').localeCompare(b.name ?? 'z', 'en', { numeric: true });
    }).forEach(guide => {
        let attributeMap = { "name": context.registerId(guide) };

        GObject.Object.list_properties.call(guide.constructor).filter(pspec => pspec.name != 'name').forEach(pspec => {
            if (guide[pspec.name] == pspec.default_value)
                return;

            let value = guide[pspec.name];
            if (GObject.type_is_a(pspec.value_type, GObject.TYPE_ENUM))
                value = enumToString(pspec.value_type, value) ?? value;

            attributeMap[pspec.name] = value;
        });
        context.appendValue("guide", attributeMap);
    });

    let constraintList = this.observe_constraints();
    let constraints = Array.from({ length: constraintList.get_n_items() }, (e_, i) => constraintList.get_item(i));

    constraints.sort((a, b) => {
        return ((a.target && getExplicitBuildableId(a.target)) ?? 'z')
            .localeCompare((b.target &&  getExplicitBuildableId(b.target)) ?? 'z', 'en', { numeric: true });
    }).forEach(constraint => {
        let attributeMap = {};
        GObject.Object.list_properties.call(constraint.constructor).forEach(pspec => {
            if (['relation', 'multiplier', 'constant', 'strength'].includes(pspec.name) && (constraint[pspec.name] == pspec.default_value))
                return;

            if (
                ['source', 'source-attribute'].includes(pspec.name) &&
                (constraint.sourceAttribute == Gtk.ConstraintAttribute.NONE) && !constraint.source
            )
                return;

            let value = constraint[pspec.name];

            if (['target', 'source'].includes(pspec.name) && !value)
                value = "super";
            else if (GObject.type_is_a(pspec.value_type, GObject.TYPE_ENUM))
                value = enumToString(pspec.value_type, value) ?? value;
            else if (value instanceof Gtk.ConstraintGuide)
                value = value.name ?? context.registerId(value);
            else if (value instanceof GObject.Object)
                value = context.registerId(value);

            attributeMap[pspec.name] = value;
        });
        context.appendValue("constraint", attributeMap);
    });

    context.pop();
};

Gtk.Dialog.prototype[$uifyCustoms] = function(context) {
    Gtk.Widget.prototype[$uifyCustoms].call(this, context);

    let actionArea = (searchWidgetForBuildableId([this], 'action_area'));
    if (!actionArea)
        return;

    let actionWidgets = [...actionArea]
        .filter(widget => isActivatable(widget) && (this.get_response_for_widget(widget) != Gtk.ResponseType.NONE));
    if (actionWidgets.length) {
        context.push("action-widgets");
        actionWidgets.forEach(widget => {
            let responseId = this.get_response_for_widget(widget);
            let response = enumToString(Gtk.ResponseType.$gtype, responseId) ?? responseId;
            context.appendValue("action-widget", { response }, context.registerId(widget));
        });
        context.pop();
    }
};

Gtk.Entry.prototype[$uifyCustoms] = function(context) {
    Gtk.Widget.prototype[$uifyCustoms].call(this, context);

    let attributes = this.attributes?.get_attributes();

    if (attributes?.length) {
        context.push("attributes");
        attributes.forEach(pangoAttribute => {
            let attributeMap = { "name": pangoAttribute.getTypeName() };
            if (pangoAttribute.start_index > Pango.ATTR_INDEX_FROM_TEXT_BEGINNING)
                attributeMap["start"] = pangoAttribute.start_index;
            if (pangoAttribute.end_index < Pango.ATTR_INDEX_TO_TEXT_END)
                attributeMap["end"] = pangoAttribute.end_index;
            // FIXME: How to retrieve the attribute value?
            attributeMap["value"] = "UNKNOWN";
            context.appendValue("attribute", attributeMap, null, true);
        });
        context.pop();
    }
};

Gtk.InfoBar.prototype[$uifyCustoms] = function(context) {
    Gtk.Widget.prototype[$uifyCustoms].call(this, context);

    let actionArea = this.get_first_child().child.get_first_child().get_next_sibling();
    let actionWidgets = [...actionArea].filter(widget => isActivatable(widget));
    if (actionWidgets.length) {
        context.push("action-widgets");
        // XXX: How to retrieve the action responses?
        actionWidgets.forEach(widget => context.appendValue("action-widget", { "response": "UNKNOWN" }, context.registerId(widget), true));
        context.pop();
    }
};

Gtk.Label.prototype[$uifyCustoms] = Gtk.Entry.prototype[$uifyCustoms];

Gtk.LevelBar.prototype[$uifyCustoms] = function(context) {
    Gtk.Widget.prototype[$uifyCustoms].call(this, context);

    // FIXME: How to retrieve custom offset names?
    let offsetNames = [Gtk.LEVEL_BAR_OFFSET_LOW, Gtk.LEVEL_BAR_OFFSET_HIGH, Gtk.LEVEL_BAR_OFFSET_FULL]
        .filter(offsetName => this.get_offset_value(offsetName)[0]);

    if (offsetNames.length) {
        context.push("offsets");
        offsetNames.forEach(offsetName => context.appendValue("offset", { "value": this.get_offset_value(offsetName)[1] }));
        context.pop();
    }
};

Gtk.ListStore.prototype[$uifyCustoms] = function(context) {
    Gtk.TreeStore.prototype[$uifyCustoms].call(this, context);

    let [hasNodes, iter] = this.get_iter_first();
    if (hasNodes) {
        context.push("data");
        do {
            context.push("row");
            for (let i = 0; i < this.get_n_columns(); i++)
                context.appendValue("col", { "id": i }, this.get_value(iter, i));
            context.pop();
        } while (this.iter_next(iter));
        context.pop();
    }
};

Gtk.SizeGroup.prototype[$uifyCustoms] = function(context) {
    context.push("widgets");
    this.get_widgets().forEach(widget => context.appendValue("widget", { "name": context.registerId(widget) }));
    context.pop();
};

Gtk.StringList.prototype[$uifyCustoms] = function(context) {
    context.push("items");
    for (let i = 0; i < this.get_n_items(); i++)
        context.appendValue("item", {}, this.get_string(i));
    context.pop();
};

Gtk.TreeStore.prototype[$uifyCustoms] = function(context) {
    context.push("columns");
    for (let i = 0; i < this.get_n_columns(); i++)
        context.appendValue("column", { "type": this.get_column_type(i).name });
    context.pop();
};

GObject.Object.prototype[$uifyChildren] = function(context) {
    getChildren(this).filter(child => !child.ignoreUI).forEach(child => {
        let childAttributeMap = {};
        if (child.type)
            childAttributeMap["type"] = child.type;
        if (child.internalChild)
            childAttributeMap["internal-child"] = child.internalChild;

        context.push(child.element ?? "child", childAttributeMap);
        context.appendObject(child.object);
        context.pop();
    });
};

Gtk.Widget.prototype[$uifyChildren] = function(context) {
    GObject.Object.prototype[$uifyChildren].call(this, context);

    // Defined in eventControllerButtons.js.
    this.getEventControllers().forEach(controller => {
        context.push("child", {});
        context.appendObject(controller);
        context.pop();
    });
};

// Try to stride the intermediate pages.
Gtk.Stack.prototype[$uifyChildren] = function(context) {
    getChildren(this).filter(child => !child.ignoreUI).forEach(({ object }) => {
        let pspecs = GObject.Object.list_properties.call(object.constructor);
        if (pspecs.every(pspec => (
            pspec.name == 'child' ||
            isSkipped(object, pspec.name) ||
            !(pspec.flags & GObject.ParamFlags.READABLE) ||
            !(pspec.flags & GObject.ParamFlags.WRITABLE) ||
            object[pspec.name] == getDefaultValue(object, pspec)
        )))
            object = object.child;

        context.push("child", {});
        context.appendObject(object);
        context.pop();
    });

    // Defined in eventControllerButtons.js.
    this.getEventControllers().forEach(controller => {
        context.push("child", {});
        context.appendObject(controller);
        context.pop();
    });
};

// XXX: How to retrieve column attributes?
Gtk.TreeViewColumn.prototype[$uifyChildren] = function(context) {
    getChildren(this).filter(child => !child.ignoreUI).forEach(child => {
        context.push(child.element ?? "child");
        context.appendObject(child.object);
        context.push("attributes");
        context.pop();
        context.pop();
    });
};

if (Adw) {
    Adw.Leaflet.prototype[$uifyChildren] = Gtk.Stack.prototype[$uifyChildren];
    Adw.Squeezer.prototype[$uifyChildren] = Gtk.Stack.prototype[$uifyChildren];
}

export const uify = function(object, context) {
    object[$uifyProps]?.(context);
    object[$uifyCustoms]?.(context);
    object[$uifyChildren]?.(context);
};
