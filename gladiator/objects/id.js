/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gio, GObject, Gtk } = imports.gi;

const $idBase = Symbol('idBase');
const $getExplicitBuildableId = Symbol('getBuildableId');

Gio.Menu.prototype[$idBase] = 'menu';
Gtk.Adjustment.prototype[$idBase] = 'adjustment';
Gtk.ConstraintGuide.prototype[$idBase] = 'guide';
Gtk.EntryBuffer.prototype[$idBase] = 'buffer';
Gtk.ListStore.prototype[$idBase] = 'liststore';
Gtk.SizeGroup.prototype[$idBase] = 'sizegroup';
Gtk.StringList.prototype[$idBase] = 'stringlist';
Gtk.TextBuffer.prototype[$idBase] = 'buffer';

Object.defineProperty(Gtk.Widget.prototype, $idBase, { get: Gtk.Widget.prototype.get_css_name });
Object.defineProperty(GObject.Object.prototype, $idBase, { get: function() {
    if (this instanceof Gio.ListModel)
        return 'model';

    return 'object';
} });

// Implicit buildable ids have the form "___object_1___".
GObject.Object.prototype[$getExplicitBuildableId] = function() {
    if (
        (this instanceof Gtk.Buildable) &&
        this.get_buildable_id() &&
        !(this.get_buildable_id().startsWith('___') && this.get_buildable_id().endsWith('___'))
    )
        return this.get_buildable_id();

    return null;
};

Gtk.ConstraintGuide.prototype[$getExplicitBuildableId] = function() {
    return this.name;
};

export const getIdBase = function(object) {
    return object[$idBase];
};

export const getExplicitBuildableId = function(object) {
    return object[$getExplicitBuildableId]?.() ?? null;
};
