/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gtk } = imports.gi;

function findWidgetForType(widgets, Klass) {
    if (!widgets.length)
        return null;

    let typedChild = widgets.find(widget => widget instanceof Klass);
    if (typedChild)
        return typedChild;

    return findWidgetForType(widgets.map(widget => [...widget]).flat(), Klass);
}

function findWidgetsForType(widgets, Klass) {
    if (!widgets.length)
        return [];

    return findWidgetsForType(widgets.map(widget => [...widget]).flat(), Klass)
        .concat(widgets.filter(widget => widget instanceof Klass));
}

Object.assign(Gtk.ShortcutsGroup.prototype, {
    add_shortcut(shortcut) {
        this.append(shortcut);
    },

    get_shortcuts() {
        return [...this].filter(widget => widget instanceof Gtk.ShortcutsShortcut);
    },

    remove_shortcut(shortcut) {
        this.remove(shortcut);
    },
});

Object.assign(Gtk.ShortcutsSection.prototype, {
    _getStack: function() {
        return findWidgetForType([this], Gtk.Stack);
    },

    add_group(group) {
        let stack = this._getStack();

        let page = stack.get_last_child();
        if (!page)
            stack.add_named(page = new Gtk.Box({ spacing: 22 }), '1');

        let column = page.get_last_child();
        if (!column)
            page.append(column = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, spacing: 22 }));

        column.append(group);
    },

    get_groups() {
        return findWidgetsForType([this._getStack()], Gtk.ShortcutsGroup);
    },

    remove_group(group) {
        let groupParent = group.parent;
        groupParent.remove(group);
        if (!groupParent.get_first_child())
            groupParent.parent.remove(groupParent);
    },
});

Object.assign(Gtk.ShortcutsWindow.prototype, {
    _getListBox: function() {
        return findWidgetForType([this._getMenuButton()], Gtk.ListBox);
    },

    _getMenuButton: function() {
        return findWidgetForType([this._getTitleStack()], Gtk.MenuButton);
    },

    _getStack: function() {
        return findWidgetForType([this.get_child()], Gtk.Stack);
    },

    _getTitleStack: function() {
        return findWidgetForType([this.get_titlebar()], Gtk.Stack);
    },

    _updateTitleStack() {
        let stack = this._getStack();
        let titleStack = this._getTitleStack();
        let menuButton = this._getMenuButton();

        if (stack.visibleChild instanceof Gtk.ShortcutsSection) {
            if (stack.pages.get_n_items() > 3) {
                titleStack.set_visible_child_name('sections');
                menuButton.set_label(stack.visibleChild.title);
            } else {
                titleStack.set_visible_child_name('title');
            }
        } else if (stack.visibleChild) {
            titleStack.set_visible_child_name('search');
        }
    },

    add_section: function(sectionName, title) {
        let stack = this._getStack();
        let listBox = this._getListBox();

        let section = new Gtk.ShortcutsSection({ sectionName, title });

        stack.add_titled(section, section.sectionName || "shortcuts", section.title);
        stack.set_visible_child(section);

        section.row = new Gtk.ListBoxRow({
            child: new Gtk.Label({
                marginStart: 6, marginEnd: 6,
                marginTop: 6, marginBottom: 6,
                label: section.title,
                xalign: 0.5,
            }),
        });

        listBox.insert(section.row, -1);
        listBox.connect('row-activated', (listBox, activatedRow) => {
            if (activatedRow == section.row)
                stack.set_visible_child(section);
        });

        this._updateTitleStack();
    },

    get_sections: function() {
        return [...this._getStack()].filter(widget => widget instanceof Gtk.ShortcutsSection);
    },

    remove_section: function(section) {
        let stack = this._getStack();
        let widgets = [...this._getStack()];
        let index = widgets.findIndex(widget => widget == section);

        if (widgets[index - 1] instanceof Gtk.ShortcutsSection)
            stack.set_visible_child(widgets[index - 1]);
        else if (widgets[index + 1] instanceof Gtk.ShortcutsSection)
            stack.set_visible_child(widgets[index + 1]);
        stack.remove(section);
        if (section.row)
            this._getListBox().remove(section.row);

        this._updateTitleStack();
    },
});
