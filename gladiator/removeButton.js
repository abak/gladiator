/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { GLib, GObject, Gtk } = imports.gi;
import { canRemove, removeChild } from './objects/children.js';

export default GObject.registerClass({
    Properties: {
        'inspected-object': GObject.ParamSpec.object(
            'inspected-object', "Inspected object", "The inspected object to remove",
            GObject.ParamFlags.READWRITE, GObject.TYPE_OBJECT
        ),
    },
    Signals: {
        'inspected-object-removed': {},
    },
}, class extends Gtk.Button {
    _init(params) {
        super._init(Object.assign({
            hasFrame: false,
            halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER,
            iconName: 'list-remove-symbolic',
            tooltipText: "Remove",
        }, params));
    }

    get inspectedObject() {
        return this._inspectedObject ?? null;
    }

    set inspectedObject(inspectedObject) {
        if (this.inspectedObject == inspectedObject)
            return;

        this._inspectedObject = inspectedObject;
        this.notify('inspected-object');

        this.sensitive = inspectedObject && canRemove(inspectedObject);
    }

    vfunc_clicked() {
        let objectToRemove = this.inspectedObject;
        if (!objectToRemove)
            return;

        GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
            removeChild(objectToRemove);
            objectToRemove = null;
            this.emit('inspected-object-removed');
        });
    }
});
