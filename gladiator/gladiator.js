/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { GLib, GObject, Gtk } = imports.gi;
const System = imports.system;

import AddButton from './addButton.js';
import ConstraintManager from './constraintManager.js';
import EventControllerButtons from './eventControllerButtons.js';
import RemoveButton from './removeButton.js';
import SizeGroupButtons from './sizeGroupButtons.js';
import UiViewer from './uiViewer.js';
import { getChildren } from './objects/children.js';
import { setCustomClasses } from './objects/classes.js';

function findWidgetForBuildableId(widgets, id) {
    if (!widgets.length)
        return null;

    let buildableChild = widgets.find(widget => widget.get_buildable_id?.() == id);
    if (buildableChild)
        return buildableChild;

    return findWidgetForBuildableId(widgets.map(widget => [...widget]).flat(), id);
}

function findWidgetForPageName(widgets, pageName) {
    if (!widgets.length)
        return null;

    let pageChild = null;

    for (let widget of widgets)
        if (widget instanceof Gtk.Stack && (pageChild = widget.get_child_by_name(pageName)))
            break;

    if (pageChild)
        return pageChild;

    return findWidgetForPageName(widgets.map(widget => [...widget]).flat(), pageName);
}

function findWidgetForType(widgets, Klass) {
    if (!widgets.length)
        return null;

    let typedChild = widgets.find(widget => widget instanceof Klass);
    if (typedChild)
        return typedChild;

    return findWidgetForType(widgets.map(widget => [...widget]).flat(), Klass);
}

function findGObjectForAddress(gObjects, address) {
    if (!gObjects.length)
        return null;

    let addressedGObject = gObjects.find(gObject => System.addressOfGObject(gObject) == address);

    if (addressedGObject)
        return addressedGObject;

    return findGObjectForAddress(gObjects.map(gObject => getChildren(gObject).map(child => child.object)).flat(), address);
}

export default GObject.registerClass({
    Properties: {
        'builder': GObject.ParamSpec.object(
            'builder', "Builder", "The initial builder",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, Gtk.Builder
        ),
        'classes': GObject.ParamSpec.jsobject(
            'classes', "Classes", "The custom JS classes",
            GObject.ParamFlags.WRITABLE | GObject.ParamFlags.CONSTRUCT_ONLY
        ),
        'inspected-object': GObject.ParamSpec.object(
            'inspected-object', "Inspected object", "The inspected object",
            GObject.ParamFlags.READWRITE, GObject.TYPE_OBJECT
        ),
        'add-button': GObject.ParamSpec.object(
            'add-button', "Add button", "The button to populate the inspected object",
            GObject.ParamFlags.READABLE, Gtk.Widget.$gtype
        ),
        'constraint-manager': GObject.ParamSpec.object(
            'constraint-manager', "Constraint manager", "The widget to manage the layout constraints of the inspected object",
            GObject.ParamFlags.READABLE, Gtk.Widget.$gtype
        ),
        'event-controller-buttons': GObject.ParamSpec.object(
            'event-controller-buttons', "Event controller buttons", "The widget to manage the event controllers of the inspected object",
            GObject.ParamFlags.READABLE, Gtk.Widget.$gtype
        ),
        'remove-button': GObject.ParamSpec.object(
            'remove-button', "Remove button", "The button to remove the inspected object",
            GObject.ParamFlags.READABLE, Gtk.Widget.$gtype
        ),
        'size-group-buttons': GObject.ParamSpec.object(
            'size-group-buttons', "Size group buttons", "The widget to manage the size groups the inspected object belongs to",
            GObject.ParamFlags.READABLE, Gtk.Widget.$gtype
        ),
        'ui-viewer': GObject.ParamSpec.object(
            'ui-viewer', "UI viewer", "The widget that displays the UI contents of the inspected object",
            GObject.ParamFlags.READABLE, Gtk.Widget.$gtype
        ),
    },
}, class extends GObject.Object {
    static addToInspector(instance) {
        let handler = Gtk.Window.get_toplevels().connect('items-changed', (list, position, removed_, added) => {
            for (let i = position; i <= position + added - 1; i++) {
                let window = list.get_item(i);
                if (!window.toString().includes('GtkInspectorWindow'))
                    continue;

                // Keep the inspector window alive.
                window.hideOnClose = true;
                list.disconnect(handler);

                let topStack = window.child;
                let objectDetails = findWidgetForPageName([topStack], 'object-details');

                let goUpButton = findWidgetForBuildableId([objectDetails], 'go_up_button');
                let goDownButton = findWidgetForBuildableId([objectDetails], 'go_down_button');
                let goPreviousButton = findWidgetForBuildableId([objectDetails], 'go_previous_button');
                let goNextButton = findWidgetForBuildableId([objectDetails], 'go_next_button');
                let buttonBox = goUpButton.parent;

                // Go to the UI parent on right click.
                let goUpGestureRightClick = new Gtk.GestureClick({ button: 3 }); // Gdk.BUTTON_SECONDARY
                goUpButton.add_controller(goUpGestureRightClick);
                goUpGestureRightClick.connect('released', gesture => {
                    if (gesture._timeout)
                        return;

                    goUpButton.emit('clicked');

                    GLib.timeout_add(GLib.PRIORITY_LOW, 100, () => {
                        if (instance.inspectedObject || !goUpButton.sensitive) {
                            delete gesture._timeout;
                            return GLib.SOURCE_REMOVE;
                        }

                        goUpButton.emit('clicked');
                        return GLib.SOURCE_CONTINUE;
                    });
                });

                // Go to the first UI child on right click.
                let goDownGestureRightClick = new Gtk.GestureClick({ button: 3 }); // Gdk.BUTTON_SECONDARY
                goDownButton.add_controller(goDownGestureRightClick);
                goDownGestureRightClick.connect('released', gesture => {
                    if (gesture._timeout || !instance.inspectedObject)
                        return;

                    let currentWidget = instance.inspectedObject;
                    let target = getChildren(currentWidget).find(
                        child => child.object instanceof Gtk.Widget && child.object.is_ancestor(currentWidget)
                    )?.object;
                    if (!target)
                        return;

                    currentWidget = currentWidget.get_first_child();
                    goDownButton.emit('clicked');

                    gesture._timeout = GLib.timeout_add(GLib.PRIORITY_LOW, 100, () => {
                        if (instance.inspectedObject) {
                            delete gesture._timeout;
                            return GLib.SOURCE_REMOVE;
                        }

                        if (target.is_ancestor(currentWidget)) {
                            if (!goDownButton.sensitive) {
                                delete gesture._timeout;
                                return GLib.SOURCE_REMOVE;
                            }

                            currentWidget = currentWidget.get_first_child();
                            goDownButton.emit('clicked');
                        } else {
                            if (!goNextButton.sensitive) {
                                delete gesture._timeout;
                                return GLib.SOURCE_REMOVE;
                            }

                            currentWidget = currentWidget.get_next_sibling();
                            goNextButton.emit('clicked');
                        }

                        return GLib.SOURCE_CONTINUE;
                    });
                });

                buttonBox.append(instance.removeButton);
                instance.removeButton.connect('clicked', () => {
                    // Activate the "Parent widget" button before the current object is removed.
                    if (instance.inspectedObject instanceof Gtk.Widget)
                        goUpButton.emit('clicked');
                    else
                        GLib.idle_add(GLib.PRIORITY_LOW, () => {
                            if (goUpButton.sensitive)
                                goUpButton.emit('clicked');
                        });
                });
                instance.removeButton.connect('inspected-object-removed', () => {
                    // Go to the first "known" ancestor object, i.e. the object to which the removed object was added.
                    // It is effective when there are intermediate built-in containers (e.g. GtkInfoBar).
                    GLib.timeout_add(GLib.PRIORITY_LOW, 100, () => {
                        if (instance.inspectedObject || !goUpButton.sensitive)
                            return GLib.SOURCE_REMOVE;

                        goUpButton.emit('clicked');
                        return GLib.SOURCE_CONTINUE;
                    });
                });

                buttonBox.append(instance.addButton);
                instance.addButton.connect('inspected-object-populated', () => {
                    // Update the "First child" button sensitivity.
                    if (instance.inspectedObject instanceof Gtk.Widget && !!instance.inspectedObject.get_first_child())
                        goDownButton.set_sensitive(true);
                });

                // GtkInspectorMiscInfo (misc-info.ui)
                let miscInfo = findWidgetForPageName([objectDetails], 'misc');
                let stack = miscInfo.parent;
                stack.add_titled(instance.constraintManager, 'constraints', "Constraints");
                stack.add_titled(instance.uiViewer, 'ui', "UI");

                instance.sizeGroupButtons.connect('inspected-object-grouped', () => {
                    // Refresh the size-groups page.
                    let [forButton, backButton] = (
                        goPreviousButton.sensitive ? [goPreviousButton, goNextButton] :
                        goNextButton.sensitive ? [goNextButton, goPreviousButton] :
                        // The inspected object is the only child.
                        [goUpButton, goDownButton]
                    );

                    forButton.emit('clicked');
                    backButton.emit('clicked');
                    stack.set_visible_child_name('size-groups');
                });

                let controllerWidget = stack.get_child_by_name('controllers');
                controllerWidget.set_layout_manager(new Gtk.BoxLayout({
                    orientation: Gtk.Orientation.VERTICAL,
                    spacing: 10,
                }));
                controllerWidget.get_first_child().set_propagate_natural_height(true);
                instance.eventControllerButtons.set_parent(controllerWidget);

                let listBox = findWidgetForType([miscInfo], Gtk.ListBox);
                let addressRow = listBox.get_first_child();
                let addressBox = addressRow.get_first_child();
                let addressLabel = addressBox.get_last_child();
                addressLabel.connect('notify::label', () => {
                    let windows = Gtk.Window.list_toplevels().filter(window => !window.toString().includes('GtkInspectorWindow'));
                    instance.inspectedObject = findGObjectForAddress(windows, addressLabel.label);

                    if ((instance.inspectedObject instanceof Gtk.Widget) && !(instance.inspectedObject instanceof Gtk.Window)) {
                        GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
                            let sizeGroupsWidget = stack.get_child_by_name('size-groups');
                            let sizeGroupsPage = stack.get_page(sizeGroupsWidget);
                            sizeGroupsPage.set_visible(true);
                            instance.sizeGroupButtons.halign = Gtk.Align.START;

                            // Gtk Inspector clears the box when the inspected object changes,
                            // so sizeGroupButtons are not packed at this moment.
                            sizeGroupsWidget.append(instance.sizeGroupButtons);
                        });
                    }

                    let constraintsWidget = stack.get_child_by_name('constraints');
                    let constraintsPage = stack.get_page(constraintsWidget);
                    constraintsPage.set_visible(instance.inspectedObject instanceof Gtk.ConstraintLayout);
                });

                break;
            }
        });
    }

    get addButton() {
        if (!this._addButton) {
            this._addButton = new AddButton();
            this.bind_property('inspected-object', this._addButton, 'inspected-object', GObject.ParamFlags.SYNC_CREATE);

            this._addButton.connect('inspected-object-populated', () => {
                this._uiViewer?.update();
            });
        }

        return this._addButton;
    }

    get constraintManager() {
        if (!this._constraintManager) {
            this._constraintManager = new ConstraintManager();
            this.bind_property('inspected-object', this._constraintManager, 'inspected-object', GObject.ParamFlags.SYNC_CREATE);
        }

        return this._constraintManager;
    }

    get eventControllerButtons() {
        if (!this._eventControllerButtons) {
            this._eventControllerButtons = new EventControllerButtons();
            this.bind_property('inspected-object', this._eventControllerButtons, 'inspected-object', GObject.ParamFlags.SYNC_CREATE);
        }

        return this._eventControllerButtons;
    }

    get removeButton() {
        if (!this._removeButton) {
            this._removeButton = new RemoveButton();
            this.bind_property('inspected-object', this._removeButton, 'inspected-object', GObject.ParamFlags.SYNC_CREATE);

            this._removeButton.connect('inspected-object-removed', () => {
                this._addButton?.update();
                this._uiViewer?.update();
            });
        }

        return this._removeButton;
    }

    get sizeGroupButtons() {
        if (!this._sizeGroupButtons) {
            this._sizeGroupButtons = new SizeGroupButtons();
            this.bind_property('inspected-object', this._sizeGroupButtons, 'inspected-object', GObject.ParamFlags.SYNC_CREATE);
        }

        return this._sizeGroupButtons;
    }

    get uiViewer() {
        if (!this._uiViewer) {
            // XXX: gtk_builder_get_translation_domain returns null for domains defined in the builder file.
            this._uiViewer = new UiViewer({ translationDomain: this.builder?.translationDomain ?? null });
            this.bind_property('inspected-object', this._uiViewer, 'inspected-object', GObject.ParamFlags.SYNC_CREATE);
        }

        return this._uiViewer;
    }

    get builder() {
        return this._builder ?? null;
    }

    set builder(builder) {
        this._builder = builder;
        this.notify('builder');

        builder?.get_objects()
            .filter(object => object instanceof Gtk.SizeGroup)
            .forEach(sizeGroup => Gtk.SizeGroup.sizeGroups.add(sizeGroup));

        builder?.get_objects()
            .filter(object => object instanceof Gtk.EventController)
            .forEach(controller => Gtk.EventController.eventControllers.add(controller));
    }

    set classes(classes) {
        if (classes)
            setCustomClasses(classes);
    }
});
